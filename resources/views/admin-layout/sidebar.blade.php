<!-- Main sidebar -->
<div class="sidebar sidebar-main">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="category-content">
                <div class="media">
                    {{--<a href="#" class="media-left"><img src="/images/logo.png"--}}
                                                        {{--class="img-circle img-sm" alt=""></a>--}}
                    <div class="media-body">
                        <span class="media-heading text-semibold">القائمة الجانبية</span>
                        <div class="text-size-mini text-muted">
                            <i class="icon-user-tie text-size-small"></i> &nbsp;{{auth()->user()->name}}
                        </div>
                    </div>

                    <div class="media-right media-middle">
                        <ul class="icons-list">
                            <li>
                                <a href="#"><i class="icon-cog3"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <!-- Main -->
                    <li class="navigation-header"><span>القائمة</span> <i class="icon-menu"
                                                                          title="Main pages"></i></li>
                    <li class="@if(Request::is('admin')) active @endif"><a href="/admin/dashboard"><i class="icon-home4"></i> <span>الرئيسيه</span></a>
                    <li class="@if(Request::is('/admin/admins')) active @endif"><a href="/admin/admins"><i class="icon-user-tie"></i> <span>المدراء</span></a></li>
                    <li class="@if(Request::is('/admin/employees')) active @endif"><a href="/admin/employees"><i class="icon-user-check"></i> <span>الموظفين</span></a></li>
                    <li class="@if(Request::is('/admin/types')) active @endif"><a href="/admin/types"><i class="icon-cabinet"></i> <span>أنواع الأجازات</span></a></li>
                    <li class="@if(Request::is('/admin/holidays')) active @endif"><a href="/admin/holidays"><i class="icon-gift"></i> <span> الأجازات</span></a></li>
                    <li class="@if(Request::is('/admin/permissions')) active @endif"><a href="/admin/permissions"><i class="icon-question3"></i> <span> الاستئذان</span></a></li>
                    <li class="@if(Request::is('/admin/courses')) active @endif"><a href="/admin/courses"><i class="icon-book"></i> <span> الدورات</span></a></li>
                    <li class="@if(Request::is('/admin/tasks')) active @endif"><a href="/admin/tasks"><i class="icon-task"></i> <span> مهام العمل</span></a></li>
                    <li class="@if(Request::is('/admin/reports')) active @endif"><a href="/admin/reports"><i class="icon-dash"></i> <span> تقارير</span></a></li>

                    {{--</li>--}}
                    {{--@can('admins', \App\User::class)--}}
                    {{--<li class='@if(Request::is('admin/users')) active @endif'>--}}
                        {{--<a href="/admin/users"><i class="icon-comments"></i> <span>الأعضاء</span></a>--}}
                    {{--</li>--}}
                    {{--@endcan--}}
                    {{--<li class="@if(Request::is('admin/states','admin/cities')) active @endif">--}}
                        {{--<a href="#"><i class="glyphicon glyphicon-map-marker"></i> <span>المناطق</span></a>--}}
                        {{--<ul>--}}
                            {{--<li><a href="/admin/states">المناطق</a></li>--}}
                            {{--<li><a href="/admin/cities">المدن</a></li>--}}
                            {{--<li><a href="/admin/towns">الأحياء</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--@can('approvedoctors', \App\Models\Doctor::class)--}}

                    {{--<li class='@if(Request::is('/admin/users/waitingApprove','/admin/users/doctorsApproved')) active @endif'>--}}
                        {{--<a href="#"><i class="icon-comments"></i> <span>بيانات طبيب / معمل</span></a>--}}
                        {{--<ul>--}}
                            {{--<li><a href="/admin/users/waitingApprove"> طلب قبول طبيب / معمل <span id="count">{{ count(auth()->user()->unreadNotifications->filter(function ($note){--}}
                              {{--return  $note->type=='App\Notifications\newDoctor';--}}
                        {{--})) }}</span></a></li>--}}
                            {{--<li><a href="/admin/users/doctorsApproved">جميع الأطباء</a></li>--}}
                            {{--<li><a href="/admin/users/labsApproved">جميع المعامل</a></li>--}}
                            {{--<li><a href="/admin/doctor/create">إضافة طبيب</a></li>--}}
                            {{--<li><a href="/admin/lab/create">إضافة معمل</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--@endcan--}}
                    {{--@can('doctors', \App\Models\Doctor::class)--}}
                    {{--<li class='@if(Request::is('admin/doctors')) active @endif'>--}}
                        {{--<a href="/admin/doctors"><i class="icon-comments"></i> <span>بيانات الطبيب</span></a>--}}
                    {{--</li>--}}
                    {{--@endcan--}}
                    {{--@can('ratings', \App\Models\Rating::class)--}}
                    {{--<li class="@if(Request::is('admin/ratings','admin/waitingApprove')) active @endif">--}}
                        {{--<a href="#"><i class="glyphicon glyphicon-map-marker"></i> <span>التقييمات</span></a>--}}
                        {{--<ul>--}}
                            {{--<li><a href="/admin/ratings">التقييات المقبولة</a></li>--}}
                            {{--<li><a href="/admin/waitingApprove">تقييمات بانتظار القبول</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--@endcan--}}
                    {{--@can('labs', \App\Models\Lab::class)--}}
                    {{--<li class='@if(Request::is('admin/labs')) active @endif'>--}}
                        {{--<a href="/admin/labs"><i class="icon-comments"></i> <span>المعامل والمختبرات</span></a>--}}
                    {{--</li>--}}
                    {{--@endcan--}}
                    {{--@can('reservations', \App\Models\Reservation::class)--}}
                    {{--<li class="@if(Request::is('admin/reservations','admin/reservations/waitingApprove')) active @endif">--}}
                        {{--<a href="#"><i class="glyphicon glyphicon-map-marker"></i> <span>الحجوزات</span></a>--}}
                        {{--<ul>--}}
                            {{--<li><a href="/admin/reservations">الحجوزات المقبولة</a></li>--}}
                            {{--<li><a href="/admin/reservations/waitingApprove">حجوزات بانتظار القبول</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--@endcan--}}
                    {{--@can('categories', \App\Models\Category::class)--}}
                        {{--<li class='@if(Request::is('admin/categories')) active @endif'>--}}
                            {{--<a href="/admin/categories"><i class="icon-comments"></i> <span>التخصصات</span></a>--}}
                        {{--</li>--}}
                    {{--@endcan--}}
                    {{--@can('companies', \App\Models\Company::class)--}}
                        {{--<li class='@if(Request::is('admin/companies')) active @endif'>--}}
                            {{--<a href="/admin/companies"><i class="icon-comments"></i> <span>شركات التأمين</span></a>--}}
                        {{--</li>--}}
                    {{--@endcan--}}
                    {{--@can('messages', \App\Models\Message::class)--}}
                        {{--<li class='@if(Request::is('send-message/1')) active @endif'>--}}
                            {{--<a href="/admin/send-message/1"><i class="icon-comments"></i> <span>التواصل مع الادارة</span></a>--}}
                        {{--</li>--}}
                    {{--@endcan--}}
                    {{--@can('admins', \App\User::class)--}}
                        {{--<li class='@if(Request::is('/admin/multimessages')) active @endif'>--}}
                            {{--<a href="/admin/multimessages"><i class="icon-comments"></i> <span>ارسال رسائل جماعيه</span></a>--}}
                        {{--</li>--}}
                    {{--@endcan--}}
                    @can('contacts', \App\Models\Contact::class)
                    <li class='@if(Request::is('admin/contacts')) active @endif'>
                        <a href="/admin/contacts"><i class="icon-comments"></i> <span>رسائل اتصل بنا</span></a>
                    </li>
                    @endcan
                    <!-- /page kits -->
                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
<!-- /main sidebar -->
