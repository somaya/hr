<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="_token" content="{{ csrf_token() }}"/>

    <title>لوحة التحكم</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="{{ Request::root() }}/admin-panel/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="{{ Request::root() }}/admin-panel/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="{{ Request::root() }}/admin-panel/assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="{{ Request::root() }}/admin-panel/assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="{{ Request::root() }}/admin-panel/assets/css/colors.css" rel="stylesheet" type="text/css">
    <link href="{{ Request::root() }}/admin-panel/assets/css/style.css" rel="stylesheet" type="text/css">
    <link href="{{ Request::root() }}/admin-panel/assets/css/fastselect.min.css" rel="stylesheet" type="text/css">
    <!-- /globa stylesheets -->

    <!-- Core JS files -->
    <script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
    <script type="text/javascript"
            src="{{ Request::root() }}/admin-panel/assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript"
            src="{{ Request::root() }}/admin-panel/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="{{ Request::root() }}/admin-panel/assets/js/main-js.js"></script>
    <script type="text/javascript" src="{{ Request::root() }}/admin-panel/assets/js/search.js"></script>
    <script type="text/javascript"
            src="{{ Request::root() }}/admin-panel/assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript"
            src="{{ Request::root() }}/admin-panel/assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript"
            src="{{ Request::root() }}/admin-panel/assets/js/plugins/visualization/d3/d3.min.js"></script>
    <script type="text/javascript"
            src="{{ Request::root() }}/admin-panel/assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script type="text/javascript"
            src="{{ Request::root() }}/admin-panel/assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript"
            src="{{ Request::root() }}/admin-panel/assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript"
            src="{{ Request::root() }}/admin-panel/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript"
            src="{{ Request::root() }}/admin-panel/assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript"
            src="{{ Request::root() }}/admin-panel/assets/js/plugins/pickers/daterangepicker.js"></script>

    <script type="text/javascript" src="{{ Request::root() }}/admin-panel/assets/js/core/app.js"></script>

{{--    <script type="text/javascript" src="{{ Request::root() }}/admin/assets/js/pages/dashboard.js"></script>--}}
<!-- /theme JS files -->
    <!--Sweet Alert style-->
    <link rel="stylesheet" type="text/css" href="{{ Request::root() }}/admin-panel/dist/sweetalert.css">
    <!--Sweet Alert Style-->
    <!--Sweet Alert Js-->
    <script src="{{ Request::root() }}/admin-panel/dist/sweetalert.min.js"></script>
    <!--Sweet Alert Js-->

    <!-- Theme JS files -->
    <script type="text/javascript"
            src="{{ Request::root() }}/admin-panel/assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript"
            src="{{ Request::root() }}/admin-panel/assets/js/plugins/forms/selects/select2.min.js"></script>

    <script type="text/javascript" src="{{ Request::root() }}/admin-panel/assets/js/pages/datatables_basic.js"></script>
    <!-- /theme JS files -->
    <script type="text/javascript"
            src="{{ Request::root() }}/admin-panel/assets/js/pages/components_modals.js"></script>

    {{--<script type="text/javascript" src="{{ Request::root() }}/admin-panel/assets/js/pages/address.js"></script>--}}
    <script src="{{ Request::root() }}/admin-panel/assets/js/pages/dropzone.js"></script>

    {{-- file input --}}
    <link href="/file_input/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
    <link href="/file_input/themes/explorer-fa/theme.css" media="all" rel="stylesheet" type="text/css"/>
    <script src="/file_input/js/plugins/sortable.js" type="text/javascript"></script>
    <script src="/file_input/js/fileinput.js" type="text/javascript"></script>
    <script src="/file_input/js/locales/fr.js" type="text/javascript"></script>
    <script src="/file_input/js/locales/es.js" type="text/javascript"></script>
    <script src="/file_input/themes/explorer-fa/theme.js" type="text/javascript"></script>
    <script src="/file_input/themes/fa/theme.js" type="text/javascript"></script>
    {{-- clock picker --}}
    {{--<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">--}}
    {{--<link rel="stylesheet" type="text/css" href="/clockpicker/dist/bootstrap-clockpicker.min.css">--}}
    {{--<script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>--}}
    {{--<script type="text/javascript" src="/clockpicker/dist/bootstrap-clockpicker.min.js"></script>--}}
    {{--<script type="text/javascript" src="/js/jquery-ui.min.js"></script>--}}
    {{--<script type="text/javascript">--}}
        {{--$(function () {--}}
            {{--$('.clockpicker').clockpicker({--}}
                {{--placement: 'top',--}}
                {{--align: 'left',--}}
                {{--donetext: 'Done'--}}
            {{--});--}}
            {{--$('.datepicker1').datepicker();--}}
        {{--});--}}
    {{--</script>--}}
    <script type="text/javascript" src="{{url('/admin-panel/fastselect-master/dist/fastselect.standalone.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/admin-panel/fastselect-master/src/fastselect.js')}}"></script>
    <script type="text/javascript" src="{{url('/admin-panel/fastselect-master/dist/fastselect.min.js')}}"></script>

    <script>
        $(function () {
            $('.multipleSelect').fastselect();
        })
    </script>
</head>
<style>

</style>
<body>
<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a href="#" style="display: block;
    text-align: center;
    padding: 5px;">
             <img src="/images/ibtdilogo.png" alt="" style="max-width: 100%;
    max-height: 100%;
    width: 100px;">
        </a>
        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a>
            </li>


        </ul>

        <ul class="nav navbar-nav navbar-right">
            <li>
                {{--<a href="{{ auth()->user()->type == null ? '/admin/users/doctorsApproved' : 'send-message/1' }}" class="hideSmall "><i class="icon-envelop4"></i>--}}
                    {{--@if(auth()->user()->msgCount(auth()->user()->id) > 0)--}}
                        {{--<span class="text-default btn-danger noti_alert">--}}
                            {{--{{auth()->user()->msgCount(auth()->user()->id)}}--}}
                        {{--</span>--}}
                    {{--@endif--}}
                    {{--الرسائل الخاصة--}}
                    {{--<span id="msgcount">{{ auth()->user()->msgCount(auth()->user()->id) }}</span>--}}
                {{--</a>--}}
            </li>
            {{--<li>--}}
                {{--<a href="/admin/messages" class="hideSmall "><i class="icon-envelop4"></i>--}}
                    {{--@if(\auth()->user()->contactcount)--}}
                        {{--<span class="text-default btn-danger noti_alert">{{ \auth()->user()->contactcount }}</span>--}}
                    {{--@endif--}}
                    {{--رسائل اتصل بنا--}}

                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="dropdown notification">--}}
                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
                            {{--<span class="hidden-xs">الاشعارات--}}
                                {{--<span id="count">{{ count(auth()->user()->unreadNotifications) }}</span>--}}
                            {{--</span><span class="caret"></span>--}}
                    {{--@if(count(auth()->user()->unreadNotifications) == 0)--}}
                        {{--<style>--}}
                            {{--#count {--}}
                                {{--background-color: #ccc !important;--}}
                            {{--}--}}
                        {{--</style>--}}
                    {{--@endif--}}
                {{--</a>--}}
                {{--<ul class="dropdown-menu notification" id="showNotification">--}}

                    {{--<!-- Menu Footer-->--}}
                    {{--@foreach(auth()->user()->notifications->take(10) as $note)--}}
                        {{--<li class="text-center">--}}
                            {{--@if($note->type=='App\Notifications\newDoctor')--}}
                                {{--<a href="/admin/users/waitingApprove" class="notify {{$note->read_at == null ? 'unread' : ''}}">--}}
                                    {{--{!! $note->data['message'] !!}--}}
                                {{--</a>--}}
                                {{--@else--}}
                                {{--<a href="#" class="notify {{$note->read_at == null ? 'unread' : ''}}">--}}
                                    {{--{!! $note->data['message'] !!}--}}
                                {{--</a>--}}

                            {{--@endif--}}
                            {{--<hr class="hr">--}}
                        {{--</li>--}}
                    {{--@endforeach--}}
                    {{--@if(count(auth()->user()->notifications) > 10)--}}
                        {{--<li><a href="/admin/showMore" class="showMore">Show More</a></li>--}}
                    {{--@endif--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--@if(\auth()->user() && \auth()->user()->role == 1)--}}
            {{--<a class="" href="/">الانتقال الى موقع حراج</a>--}}
            {{--@endif--}}
            {{--</li>--}}

            <li>
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    تسجيل الخروج
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->

{{--<script type="text/javascript" src="{{ Request::root() }}/js/tinymce/tinymce.min.js"></script>--}}
{{--<script>tinymce.init({selector: 'textarea'});</script>--}}
<script>
    var count = $('#count'), c;
    $('.notification').click(function () {
//        alert('clicked');
        $.get('/MarkAllSeen', function () {
//            alert('readed');
            setTimeout(function () {
                count.html(0);
                count.style.backgroundColor = '#ccc !important';
                $('.unread').each(function () {
                    $(this).removeClass('unread');
                });
            }, 5000); // remove class after five second
        });  // ajax
    });
</script>
<style>
    #count {
        background-color: red;
        padding: 0 4px;
        border-radius: 50%;
    }
</style>