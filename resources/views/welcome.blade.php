
<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> الموظفين</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="/admin-panel/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="/admin-panel/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="/admin-panel/assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="/admin-panel/assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="/admin-panel/assets/css/colors.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="/admin-panel/assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="/admin-panel/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="/admin-panel/assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="/admin-panel/assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="/admin-panel/assets/js/plugins/forms/styling/uniform.min.js"></script>

    <script type="text/javascript" src="/admin-panel/assets/js/core/app.js"></script>
    <script type="text/javascript" src="/admin-panel/assets/js/pages/login.js"></script>
    <!-- /theme JS files -->

</head>

<body class="login-cover">
@include('admin.message')


<!-- Page container -->
<div class="page-container login-container">


    <!-- Page content -->
    <div class="page-content">



        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                <!-- Tabbed form -->
                <div class="tabbable panel login-form width-400">
                    <ul class="nav nav-tabs nav-justified">

                        {{--<li class="active"><a href="#basic-tab1" data-toggle="tab"><h6><i class="icon-checkmark3 position-left"></i> مسجل بالفعل؟</h6></a></li>--}}
                        {{--<li><a href="#basic-tab2" data-toggle="tab"><h6><i class="icon-plus3 position-left"></i> انشاء حساب</h6></a></li>--}}
                    </ul>

                    <div class="tab-content panel-body">
                        <div class="tab-pane fade in active" id="basic-tab1">
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="text-center">
                                    <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
                                    <h5 class="content-group">تسجيل الدخول <small class="display-block">بياناتك</small></h5>
                                </div>

                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="البريد الالكترونى" name="email" required="required">
                                    <div class="form-control-feedback">
                                        <i class="icon-user text-muted"></i>
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="كلمة المرور" name="password" required="required">
                                    <div class="form-control-feedback">
                                        <i class="icon-lock2 text-muted"></i>
                                    </div>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>



                                <div class="form-group">
                                    <button type="submit" class="btn bg-blue btn-block">تسجيل الدخول <i class="icon-arrow-left13 position-right"></i></button>
                                </div>
                            </form>

                        </div>


                    </div>
                </div>
                <!-- /tabbed form -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
