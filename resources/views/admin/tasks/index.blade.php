@extends('admin-layout.app')
@section('content')

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">الرئيسية</span> -قائمة مهام العمل
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="icon-home2 position-left"></i> الرئيسية</a></li>
                <li class="active">قائمة مهام العمل
                </li>
            </ul>

        </div>
    </div>
    <!-- /page header -->


    @include('admin.message')
    <!-- Content area -->
    <div class="content">


        <!-- State saving -->
        <div class="panel panel-flat">
            <div class="panel-heading">

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <div class="contant">
                    <strong>ابحث بالتاريخ :</strong>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">من: </label>
                        <div class="col-lg-4">
                            <input type="date" id="start_date" class="form-control m-input">

                        </div>
                        <label class="col-lg-2 col-form-label">الى : </label>
                        <div class="col-lg-4">
                            <input type="date" id="end_date" class="form-control m-input">

                        </div>
                    </div>
                    <button class="btn btn-success" id="task_search_button">ابحث</button>


                </div>

            </div>

            <table class="table datatable-save-state" id="task_tbl" >
                <thead>
                <tr>
                    <th class="text-left">#</th>
                    <th>إسم الموظف</th>
                    <th>تاريخ المهمة</th>
                    <th>عدد الايام</th>
                    <th>مكان المهمة</th>
                    <th>مضاف بواسطة</th>
                    <th class="text-left">الاجراء المتخذ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($tasks as $index=>$task)
                    <tr>
                        <td>{{ ++$index }}</td>
                        <td>{{ $task->employee->name }} </td>
                        <td>{{ $task->start }}</td>
                        <td>{{ $task->number_of_days }}</td>
                        <td>{{ $task->location }}</td>
                        <td>{{ $task->hr->name }}</td>


                        <td class="text-center">
                            <ul class="icons-list">
                                <li class="text-primary-600">
                                    <a title="تعديل" href="/admin/tasks/{{ $task->id }}/edit"><i class="icon-pencil7"></i></a>
                                </li>


                                <li class="text-danger-600">
                                    <a title="حذف المهمة" onclick="return false;" object_id="{{ $task->id }}"
                                       delete_url="/admin/tasks/{{ $task->id }}"
                                       class="sweet_warning delete_user" href="#">
                                        <i class="icon-trash"></i></a>
                                </li>



                            </ul>
                        </td>

                    </tr>
                @endforeach

                </tbody>
            </table>
            <a href="/admin/task/export">
                <button type="button" name="button" style="margin: 20px;"
                        class="btn btn-danger position-left ">Excel <i class=" icon-download position-left"></i>
                </button>

            </a>
            <a href="/admin/task/pdf">
                <button type="button" name="button" style="margin: 20px;"
                        class="btn btn-danger position-left ">Pdf <i class=" icon-download position-left"></i>
                </button>

            </a>


        </div>
        <!-- /state saving -->


    </div>
    <!-- /content area -->
@endsection
