<html>
<head>
    <meta charset="utf-8">
    <title>Permissions </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    {{--<link type="text/css" rel="stylesheet" src="/website/assets/css/style_ar.css">--}}

    <style>
        * { font-family:  Noto Naskh Arabic, DejaVu Sans,  sans-serif;
            direction: rtl;


        }

        @font-face {
            src: url("http://experimenting.in/css3/webfonts/hindifontsdemo/gargi.ttf") format('truetype');
            font-family: "gargi";
        }

        /*font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;*/

        .contact-seller-header {
            background-color: #3c7bed;
            padding: 15px;/*NEW 10 => 15*/
            color: #fff;
            font-size: 14px;
        }
        .contact-seller-header a{
            color: #fff !important;
        }




    </style>
    {{--<link rel="stylesheet" href="{{ asset("website/assets/css/style.css") }}">--}}

</head>
<body>
<table style="direction: rtl" >
    <thead>
    <tr>
        <th>#</th>
        <th>اسم الموظف</th>
        <th>تاريخ المهمه</th>
        <th>عدد الايام </th>
        <th>مكان المهمه</th>
        <th>مضاف بواسطة</th>

    </tr>
    </thead>
    <tbody>
    @foreach($tasks as $index=> $task)


        <tr>

            <td>{{ ++$index }}</td>
            <td>{{$task->employee->name}}</td>
            <td>{{$task->start}} </td>
            <td>{{$task->number_of_days}} </td>
            <td>{{$task->location}} </td>
            <td>{{$task->hr->name}} </td>


        </tr>
    @endforeach

    </tbody>
</table>

</body>
</html>