<html>
<body>
<table style="direction: rtl" >
    <thead>
    <tr>
        <th>#</th>
        <th>اسم الموظف</th>
        <th>تاريخ المهمه</th>
        <th>عدد الايام </th>
        <th>مكان المهمه</th>
        <th>مضاف بواسطة</th>

    </tr>
    </thead>
    <tbody>
    @foreach($tasks as $index=> $task)


        <tr>

            <td>{{ ++$index }}</td>
            <td>{{$task->employee->name}}</td>
            <td>{{$task->start}} </td>
            <td>{{$task->number_of_days}} </td>
            <td>{{$task->location}} </td>
            <td>{{$task->hr->name}} </td>


        </tr>
    @endforeach

    </tbody>
</table>

</body>
</html>