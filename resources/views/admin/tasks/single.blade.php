@extends('admin-layout.app')
@section('styles')
    <link href="{{ asset('admin-panel/assets/css/bootstrap-select.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <script type="text/javascript" src="/adminPanel/assets/js/checkboxes_radios.js"></script>
    <script type="text/javascript" src="/adminPanel/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="/adminPanel/assets/js/countries.js"></script>

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">{{ isset($task) ? 'تعديل' : 'إضافة' }}
                        مهمة عمل </span>
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="icon-home2 position-left"></i> الرئيسية</a></li>
                <li class="active">{{ isset($task) ? 'تعديل' : 'إضافة' }} مهمة عمل</li>
            </ul>

        </div>
    </div>
    <!-- /page header -->
    <div class="content">
    @include('admin.message')
    <!-- Form validation -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
                <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>

            <div class="panel-body">
            <form class="form-horizontal form-validate-jquery" method="post"
                  action="/admin/tasks{{ isset($task) ? '/'.$task->id : '' }}" novalidate="novalidate" enctype="multipart/form-data">
                {!! isset($task) ? '<input type="hidden" name="_method" value="PUT">' : '' !!}
                {{ csrf_field() }}
                <fieldset class="content-group">
                    <legend class="text-bold"></legend>
                    @if(!isset($task))
                        <input type="hidden" name="employee" value="{{$employee->id}}">
                    @endif



                    <!-- Basic text input -->


                    
                    <div class="form-group{{ $errors->has('start') ? ' has-error' : '' }}">
                        <label class="control-label col-lg-3">تاريخ البدء
                            <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="date" name="start" class="form-control" required="required"
                                   placeholder="ضع تاريخ البدء " aria-required="true"
                                   value="{{ isset($task) ? $task->start : old('start')}}">
                            @include('layouts.error', ['input' => 'start'])
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('number_of_days') ? ' has-error' : '' }}">
                        <label class="control-label col-lg-3">عدد الايام
                            <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="number" name="number_of_days" class="form-control" required="required"
                                   placeholder="ضع عدد الايام" aria-required="true"
                                   value="{{ isset($task) ? $task->number_of_days : old('number_of_days')}}">
                            @include('layouts.error', ['input' => 'number_of_days'])
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                        <label class="control-label col-lg-3">المكان<span
                                    class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="location" class="form-control" id="location" required="required"
                                   placeholder="المكان" aria-required="true"
                                   value="{{ isset($task) ? $task->location : old('location')}}">
                            @include('layouts.error', ['input' => 'location'])
                        </div>
                    </div>

                    

                </fieldset>



                <div class="text-left">
                    <button type="submit" class="btn btn-primary">
                        <i class=" icon-arrow-right7 position-left"></i> {{ isset($task) ? 'تعديل' : 'إضافة' }}
                    </button>
                    <button type="reset" class="btn btn-default" id="reset">استعادة
                        <i class="icon-reload-alt position-right"></i></button>
                    <a  class="btn btn-primary">
                        <i class=" icon-arrow-left7 position-left"></i> الرجوع
                    </a>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

    </div>

@endsection