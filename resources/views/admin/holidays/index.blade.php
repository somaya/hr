@extends('admin-layout.app')
@section('content')

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">الرئيسية</span> -قائمة الأجازات
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="icon-home2 position-left"></i> الرئيسية</a></li>
                <li class="active">قائمة الأجازات
                </li>
            </ul>

        </div>
    </div>
    <!-- /page header -->


    @include('admin.message')
    <!-- Content area -->
    <div class="content">


        <!-- State saving -->
        <div class="panel panel-flat">
            <div class="panel-heading">

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <div class="contant">
                    <strong>ابحث بالتاريخ :</strong>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">من: </label>
                        <div class="col-lg-4">
                            <input type="date" id="start_date" class="form-control m-input">

                        </div>
                        <label class="col-lg-2 col-form-label">الى : </label>
                        <div class="col-lg-4">
                            <input type="date" id="end_date" class="form-control m-input">

                        </div>
                    </div>
                    <button class="btn btn-success" id="holiday_search_button">ابحث</button>


                </div>

            </div>

            <table class="table datatable-save-state" id="holiday_tbl" >
                <thead>
                <tr>
                    <th class="text-left">#</th>
                    <th>إسم الموظف</th>
                    <th>تاريخ البدء</th>
                    <th>تاريخ النهايه</th>
                    <th>مضاف بواسطة</th>
                    <th>نوع الأجازه</th>
                    <th class="text-left">الاجراء المتخذ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($holidays as $index=>$holiday)
                    <tr>
                        <td>{{ ++$index }}</td>
                        <td>{{ $holiday->employee->name }} </td>
                        <td>{{ $holiday->start }}</td>
                        <td>{{ $holiday->end }}</td>
                        <td>{{ $holiday->hr->name }}</td>
                        <td>{{ $holiday->type->name }}</td>


                        <td class="text-center">
                            <ul class="icons-list">
                                <li class="text-primary-600">
                                    <a title="تعديل" href="/admin/holidays/{{ $holiday->id }}/edit"><i class="icon-pencil7"></i></a>
                                </li>


                                <li class="text-danger-600">
                                    <a title="حذف الأجازه" onclick="return false;" object_id="{{ $holiday->id }}"
                                       delete_url="/admin/holidays/{{ $holiday->id }}"
                                       class="sweet_warning delete_user" href="#">
                                        <i class="icon-trash"></i></a>
                                </li>



                            </ul>
                        </td>

                    </tr>
                @endforeach

                </tbody>
            </table>
            <a href="/admin/holiday/export">
                <button type="button" name="button" style="margin: 20px;"
                        class="btn btn-danger position-left ">Excel <i class=" icon-download position-left"></i>
                </button>

            </a>
            <a href="/admin/holiday/pdf">
                <button type="button" name="button" style="margin: 20px;"
                        class="btn btn-danger position-left ">Pdf <i class=" icon-download position-left"></i>
                </button>

            </a>

        </div>
        <!-- /state saving -->


    </div>
    <!-- /content area -->
@endsection
