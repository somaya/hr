@extends('admin-layout.app')
@section('content')

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">الرئيسية</span> -قائمة الموظفين
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="icon-home2 position-left"></i> الرئيسية</a></li>
                <li class="active">قائمة الموظفين
                </li>
            </ul>

        </div>
    </div>
    <!-- /page header -->


    @include('admin.message')
    <!-- Content area -->
    <div class="content">


        <!-- State saving -->
        <div class="panel panel-flat">
            <div class="panel-heading">

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">

            </div>

            <table class="table datatable-save-state" >
                <thead>
                <tr>
                    <th class="text-left">#</th>
                    <th>إسم الموظف</th>
                    <th>الوظيفه</th>
                    <th>مضاف بواسطة</th>
                    <th>الحضور</th>
                    <th>الانصراف</th>
                    <th>ساعات التأخير</th>
                    <th>تاريخ العضوية</th>
                    <th class="text-left">الاجراء المتخذ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($employees as $index=>$employee)


                    <tr>
                        <td>{{ ++$index }}</td>
                        <td>{{ $employee->name }} </td>
                        <td>{{ $employee->title }}</td>
                        <td>{{ $employee->hr->name }}</td>
                        <td>
                            @if($employee->dayAttendance() && $employee->dayAttendance()->attend==1)
                                {{$employee->dayAttendance()->login}}
                            @endif
                            @if($employee->dayAttendance() && $employee->dayAttendance()->attend==2)
                                    غياب
                            @endif
                             @if($employee->dayAttendance() && $employee->dayAttendance()->attend==null)
                                 {{$employee->dayAttendance()->status}}
                             @endif
                        <td>{{ $employee->dayAttendance() ?$employee->dayAttendance()->logout:'' }}</td>
                        <td>{{ $employee->dayAttendance() ? intdiv($employee->dayAttendance()->delay, 60).':'. ($employee->dayAttendance()->delay % 60):'' }}</td>
                        <td >{{ $employee->created_at->diffForHumans() }}</td>


                        <td class="text-center">
                            <ul class="icons-list">
                                <li class="text-primary-600">
                                    <a title="تعديل" href="/admin/employees/{{ $employee->id }}/edit"><i class="icon-pencil7"></i></a>
                                </li>
                                @if(!$employee->dayAttendance()  )
                                <li class="text-primary-600">
                                    <a title="حضور" href="/admin/employees/{{ $employee->id }}/login"><i class="icon-circle-right2"></i></a>
                                </li>
                                <li class="text-danger-600">
                                    <a title="غياب" href="/admin/employees/{{ $employee->id }}/absent"><i class="icon-cancel-circle2"></i></a>
                                </li>
                                @endif
                                @if($employee->dayAttendance() && $employee->dayAttendance()->attend==1 && $employee->dayAttendance()->logout==null)
                                <li class="text-primary-600">
                                    <a title="انصراف" href="/admin/employees/{{ $employee->id }}/logout"><i class="icon-circle-left2"></i></a>
                                </li>
                                @endif

                                <li class="text-danger-600">
                                    <a title="حذف الموظف" onclick="return false;" object_id="{{ $employee->id }}"
                                       delete_url="/admin/employees/{{ $employee->id }}"
                                       class="sweet_warning delete_user" href="#">
                                        <i class="icon-trash"></i></a>
                                </li>
                                <li class="text-primary-600"><a title="اضافة أجازة"  href="/admin/holidays/{{$employee->id}}/create"><i class="icon-gift"></i></a></li>
                                <li class="text-warning-600 "><a title="اضافة استئذان"  href="/admin/permissions/{{$employee->id}}/create"><i class="icon-question3"></i></a></li>
                                <li class="text-primary-600"><a title=" اضافة دورة"  href="/admin/courses/{{$employee->id}}/create"><i class="icon-book"></i></a></li>
                                <li class="text-primary-600"><a title="اضافة مهمة عمل"  href="/admin/tasks/{{$employee->id}}/create"><i class="icon-task"></i></a></li>



                            </ul>
                        </td>

                    </tr>
                @endforeach

                </tbody>
            </table>
            <a href="/admin/employees/create">
                <button type="button" name="button" style="margin: 20px;"
                        class="btn btn-success pull-right">اضافة موظف <i class="icon-arrow-left13 position-right"></i>
                </button>

            </a>
        </div>
        <!-- /state saving -->


    </div>
    <!-- /content area -->
@endsection
