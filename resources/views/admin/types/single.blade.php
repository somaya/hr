@extends('admin-layout.app')
@section('content')
    <script type="text/javascript" src="/adminPanel/assets/js/checkboxes_radios.js"></script>
    <script type="text/javascript" src="/adminPanel/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="/adminPanel/assets/js/countries.js"></script>

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">{{ isset($type) ? 'تعديل' : 'إضافة' }}
                        نوع أجازه </span>
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="icon-home2 position-left"></i> الرئيسية</a></li>
                <li class="active">{{ isset($type) ? 'تعديل' : 'إضافة' }} نوع أجازه</li>
            </ul>

        </div>
    </div>
    <!-- /page header -->
    <div class="content">
    @include('admin.message')
    <!-- Form validation -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
                <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>

            <div class="panel-body">
            <form class="form-horizontal form-validate-jquery" method="post"
                  action="/admin/types{{ isset($type) ? '/'.$type->id : '' }}" novalidate="novalidate">
                {!! isset($type) ? '<input type="hidden" name="_method" value="PUT">' : '' !!}
                {{ csrf_field() }}
                <fieldset class="content-group">
                    <legend class="text-bold"></legend>

                    <!-- Basic text input -->
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="control-label col-lg-3">الاسم <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="name" class="form-control" required="required"
                                   placeholder="ضع إسم الاجازه هنا" aria-required="true"
                                   value="{{ isset($type) ? $type->name : old('name')}}">
                            @include('layouts.error', ['input' => 'name'])
                        </div>
                    </div>


                    




                </fieldset>



                <div class="text-left">
                    <button type="submit" class="btn btn-primary">
                        <i class=" icon-arrow-right7 position-left"></i> {{ isset($type) ? 'تعديل' : 'إضافة' }}
                    </button>
                    <button type="reset" class="btn btn-default" id="reset">استعادة
                        <i class="icon-reload-alt position-right"></i></button>
                    <a  class="btn btn-primary">
                        <i class=" icon-arrow-left7 position-left"></i> الرجوع
                    </a>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

    </div>

@endsection