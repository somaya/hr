@extends('admin-layout.app')
@section('content')

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">الرئيسية</span> - أنواع الاجازات
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="icon-home2 position-left"></i> الرئيسية</a></li>
                <li class="active">أنواع الاجازات
                </li>
            </ul>

        </div>
    </div>
    <!-- /page header -->


    @include('admin.message')
    <!-- Content area -->
    <div class="content">


        <!-- State saving -->
        <div class="panel panel-flat">
            <div class="panel-heading">

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">

            </div>

            <table class="table datatable-save-state" >
                <thead>
                <tr>
                    <th class="text-left">#</th>
                    <th>نوع الاجازه</th>
                    <th class="text-left">الاجراء المتخذ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($types as $index=>$type)
                    <tr>
                        <td>{{ ++$index }}</td>
                        <td>{{ $type->name }} </td>
                        <td  >
                            <ul class="icons-list">
                                <li class="text-primary-600"><a title="تعديل"
                                                                href="/admin/types/{{ $type->id }}/edit"><i
                                                class="icon-pencil7"></i></a></li>



                                <li class="text-danger-600">
                                    <a title="حذف نوع الاجازه" onclick="return false;" object_id="{{ $type->id }}"
                                       delete_url="/admin/types/{{ $type->id }}"
                                       class="sweet_warning delete_user" href="#">
                                        <i class="icon-trash"></i></a>
                                </li>
                                {{--<li class="help-block"><a title="{{ $user->role==1 ? 'اجعله عضو عادى' : 'اجعله مدير' }}" href="/admin/users/admin_user/{{ $user->id }}"><i class="icon-user-tie"></i></a></li>--}}




                            </ul>
                        </td>

                    </tr>
                @endforeach

                </tbody>
            </table>
            <a href="/admin/types/create">
                <button type="button" name="button" style="margin: 20px;"
                        class="btn btn-success pull-right">اضافة نوع أجازه <i class="icon-arrow-left13 position-right"></i>
                </button>

            </a>
        </div>
        <!-- /state saving -->


    </div>
    <!-- /content area -->
@endsection
