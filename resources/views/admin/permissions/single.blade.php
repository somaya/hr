@extends('admin-layout.app')
@section('styles')
    <link href="{{ asset('admin-panel/assets/css/bootstrap-select.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <script type="text/javascript" src="/adminPanel/assets/js/checkboxes_radios.js"></script>
    <script type="text/javascript" src="/adminPanel/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="/adminPanel/assets/js/countries.js"></script>

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">{{ isset($permission) ? 'تعديل' : 'إضافة' }}
                        استئذان </span> 
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="icon-home2 position-left"></i> الرئيسية</a></li>
                <li class="active">{{ isset($permission) ? 'تعديل' : 'إضافة' }} استئذان</li>
            </ul>

        </div>
    </div>
    <!-- /page header -->
    <div class="content">
    @include('admin.message')
    <!-- Form validation -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
                <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>

            <div class="panel-body">
            <form class="form-horizontal form-validate-jquery" method="post"
                  action="/admin/permissions{{ isset($permission) ? '/'.$permission->id : '' }}" novalidate="novalidate" enctype="multipart/form-data">
                {!! isset($permission) ? '<input type="hidden" name="_method" value="PUT">' : '' !!}
                {{ csrf_field() }}
                <fieldset class="content-group">
                    <legend class="text-bold"></legend>
                    @if(!isset($permission))
                        <input type="hidden" name="employee" value="{{$employee->id}}">
                    @endif



                    <!-- Basic text input -->
                    <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                        <label class="control-label col-lg-3">نوع الاستئذان<span class="text-danger">*</span></label>
                        <div class="col-lg-9">

                            <select name="type" class="form-control" required="required" aria-required="true">
                                <option value="">اختر نوع الاستئذان</option>
                                    <option value="بعذر" {{isset($permission) && $permission->type=='بعذر' ?'selected':''}} {{old('type')=='بعذر' ?'selected':''}}>بعذر</option>
                                    <option value="بدون عذر" {{isset($permission) && $permission->type=='بدون عذر' ?'selected':''}} {{old('type')=='بدون عذر' ?'selected':''}}>بدون عذر</option>
                                    <option value="مكافأه" {{isset($permission) && $permission->type=='مكافأه' ?'selected':''}} {{old('type')=='مكافأه' ?'selected':''}}>مكافأه</option>

                            </select>
                            @include('layouts.error', ['input' => 'type'])
                        </div>
                    </div>

                    
                    <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                        <label class="control-label col-lg-3">التاريخ
                            <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="date" name="date" class="form-control" required="required"
                                   placeholder="ضع التاريخ " aria-required="true"
                                   value="{{ isset($permission) ? $permission->date : old('date')}}">
                            @include('layouts.error', ['input' => 'date'])
                        </div>
                    </div>


                    

                </fieldset>



                <div class="text-left">
                    <button type="submit" class="btn btn-primary">
                        <i class=" icon-arrow-right7 position-left"></i> {{ isset($permission) ? 'تعديل' : 'إضافة' }}
                    </button>
                    <button type="reset" class="btn btn-default" id="reset">استعادة
                        <i class="icon-reload-alt position-right"></i></button>
                    <a  class="btn btn-primary">
                        <i class=" icon-arrow-left7 position-left"></i> الرجوع
                    </a>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

    </div>

@endsection