<html>
<body>
<table style="direction: rtl" >
    <thead>
    <tr>
        <th>#</th>
        <th>اسم الموظف</th>
        <th>الدوره</th>
        <th>تاريخ البدء</th>
        <th>تاريخ النهايه</th>
        <th>مكان الدوره</th>
        <th>الجهه المانحه</th>
        <th>مضاف بواسطة</th>

    </tr>
    </thead>
    <tbody>
    @foreach($courses as $index=> $course)


        <tr>

            <td>{{ ++$index }}</td>
            <td>{{$course->employee->name}}</td>
            <td>{{$course->title}}</td>
            <td>{{$course->start}} </td>
            <td>{{$course->end}} </td>
            <td>{{$course->location}} </td>
            <td>{{$course->donor}} </td>
            <td>{{$course->hr->name}} </td>


        </tr>
    @endforeach

    </tbody>
</table>

</body>
</html>