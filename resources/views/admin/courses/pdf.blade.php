<html>
<head>
    <meta charset="utf-8">
    <title>Permissions </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    {{--<link type="text/css" rel="stylesheet" src="/website/assets/css/style_ar.css">--}}

    <style>
        * { font-family:  Noto Naskh Arabic, DejaVu Sans,  sans-serif;
            direction: rtl;


        }

        @font-face {
            src: url("http://experimenting.in/css3/webfonts/hindifontsdemo/gargi.ttf") format('truetype');
            font-family: "gargi";
        }

        /*font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;*/

        .contact-seller-header {
            background-color: #3c7bed;
            padding: 15px;/*NEW 10 => 15*/
            color: #fff;
            font-size: 14px;
        }
        .contact-seller-header a{
            color: #fff !important;
        }




    </style>
    {{--<link rel="stylesheet" href="{{ asset("website/assets/css/style.css") }}">--}}

</head>
<body>
<table style="direction: rtl" >
    <thead>
    <tr>
        <th>#</th>
        <th>اسم الموظف</th>
        <th>الدوره</th>
        <th>تاريخ البدء</th>
        <th>تاريخ النهايه</th>
        <th>مكان الدوره</th>
        <th>الجهه المانحه</th>
        <th>مضاف بواسطة</th>

    </tr>
    </thead>
    <tbody>
    @foreach($courses as $index=> $course)


        <tr>

            <td>{{ ++$index }}</td>
            <td>{{$course->employee->name}}</td>
            <td>{{$course->title}}</td>
            <td>{{$course->start}} </td>
            <td>{{$course->end}} </td>
            <td>{{$course->location}} </td>
            <td>{{$course->donor}} </td>
            <td>{{$course->hr->name}} </td>


        </tr>
    @endforeach

    </tbody>
</table>

</body>
</html>