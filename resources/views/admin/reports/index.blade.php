@extends('admin-layout.app')
@section('content')

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">الرئيسية</span>- تقارير
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="icon-home2 position-left"></i> الرئيسية</a></li>
                <li class="active">تقارير
                </li>
            </ul>

        </div>
    </div>
    <!-- /page header -->


    @include('admin.message')
    <!-- Content area -->
    <div class="content">

        <!-- State saving -->
        <div class="panel panel-flat">
            <div class="panel-heading">

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <strong>كل الغائبين خلال الشهر</strong>

            </div>

            <table class="table datatable-save-state" >
                <thead>
                <tr>
                    <th class="text-left">#</th>
                    <th>إسم الموظف</th>
                    <th> اليوم</th>
                </tr>
                </thead>
                <tbody>
                @foreach($all_absents as $index=>$absent)
                    <tr>
                        <td>{{ ++$index }}</td>
                        <td>{{ $absent->employee->name }} </td>
                        <td>{{ $absent->day }} </td>
                    </tr>
                @endforeach

                </tbody>
            </table>

        </div>
        <!-- /state saving -->


    </div>
    <div class="content">

        <!-- State saving -->
        <div class="panel panel-flat">
            <div class="panel-heading">

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <strong>غيابات الموظفين خلال الشهر</strong>

            </div>

            <table class="table datatable-save-state" >
                <thead>
                <tr>
                    <th class="text-left">#</th>
                    <th>إسم الموظف</th>
                    <th> عدد ايام الغياب</th>
                </tr>
                </thead>
                <tbody>
                @foreach($employee_absences as $index=>$absent)
                    <tr>
                        <td>{{ ++$index }}</td>
                        <td>{{ $absent->name }} </td>
                        <td>{{ $absent->total }} </td>
                    </tr>
                @endforeach

                </tbody>
            </table>

        </div>
        <!-- /state saving -->


    </div>
    <div class="content">

        <!-- State saving -->
        <div class="panel panel-flat">
            <div class="panel-heading">

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <strong>تأخيرات الموظفين خلال الشهر</strong>

            </div>

            <table class="table datatable-save-state" >
                <thead>
                <tr>
                    <th class="text-left">#</th>
                    <th>إسم الموظف</th>
                    <th> عدد ساعات التأخير</th>
                </tr>
                </thead>
                <tbody>
                @foreach($employee_delays as $index=>$absent)
                    <tr>
                        <td>{{ ++$index }}</td>
                        <td>{{ $absent->name }} </td>
                        <td>{{ intdiv($absent->total, 60).':'. ($absent->total % 60) }} </td>
                    </tr>
                @endforeach

                </tbody>
            </table>

        </div>
        <!-- /state saving -->


    </div>

    <!-- /content area -->
@endsection
