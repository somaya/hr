@if(Session::has('error'))
    <span class="help-block alert-danger">
        <strong>{{ Session::get('error') }}</strong>
    </span>
@endif