<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    protected $table = 'holidays';
    protected $guarded = [];

    public $timestamps = true;
    public function hr()
    {
        return $this->belongsTo(User::class, 'hr_id');
    }
    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }
    public function type()
    {
        return $this->belongsTo(Holiday_type::class, 'type_id');
    }
}
