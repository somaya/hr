<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employees';
    protected $guarded = [];

    public $timestamps = true;
    public function hr()
    {
        return $this->belongsTo(User::class, 'hr_id');
    }
    public function dayAttendance(){
       return Attendence::where('day',Carbon::now()->format('Y-m-d'))
            ->where('employee_id',$this->id)->first();

    }

}
