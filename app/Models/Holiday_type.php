<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Holiday_type extends Model
{
    protected $table = 'holiday_types';
    protected $guarded = [];

    public $timestamps = true;

}
