<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Attendence extends Model
{
    protected $table = 'attendences';
    protected $guarded = [];

    public $timestamps = true;
    public function hr()
    {
        return $this->belongsTo(User::class, 'hr_id');
    }
    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }
}
