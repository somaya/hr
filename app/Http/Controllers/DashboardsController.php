<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Company;
use App\Models\Employee;
use App\User;

class DashboardsController extends Controller
{
    public function index(){
        $users = User::count();
        $employees = Employee::count();
//        $categories = Category::count();
//        $companies = Company::count();
        return view('admin.dashboard',compact('users','employees'));
    }
}
