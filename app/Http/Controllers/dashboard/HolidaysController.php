<?php

namespace App\Http\Controllers\dashboard;

use App\Exports\HolidaysExport;
use App\Http\Controllers\Controller;
use App\Models\Attendence;
use App\Models\Employee;
use App\Models\Holiday;
use App\Models\Holiday_type;
use DateTime;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class HolidaysController extends Controller
{
    public function index()
    {
        $holidays=Holiday::OrderBy('created_at','desc')->get();
        return view('admin.holidays.index',compact('holidays'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $types=Holiday_type::all();
        $employee=Employee::find($id);

        return view('admin.holidays.single',compact('employee','types'));
    }

    public function show()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
                'type' => 'required',
                'start' => 'required',
                'end' => 'required',

            ]
        );
        Holiday::create([
            'employee_id'=>$request->employee,
            'type_id'=>$request->type,
            'start'=>$request->start,
            'end'=>$request->end,
            'hr_id'=>auth()->id(),
        ]);
        //delete absence if exist in this date
        $begin = new DateTime( $request->start );
        $end   = new DateTime( $request->end );
        for($i = $begin; $i <= $end; $i->modify('+1 day')){
            Attendence::where('employee_id',$request->employee)
                ->where('day',$i->format("Y-m-d"))->delete();
            // record status
            Attendence::create([
                'employee_id'=>$request->employee,
                'status'=>'أجازة',
                'day'=>$i->format("Y-m-d"),
                'hr_id'=>auth()->id()
            ]);
        }

        return redirect('/admin/employees')->with('success', 'تم اضافة الأجازه بنجاح');
    }
    public function export()
    {
        return Excel::download(new HolidaysExport, 'holidays.xlsx');
    }
    public function pdf(){
        $holidays= Holiday::with(['employee','hr','type'])->get();


        $pdf = PDF::loadView('admin.holidays.pdf', compact('holidays'));
        return $pdf->stream('holidays.pdf');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function edit($id)
    {
        $holiday=Holiday::find($id);
        $types=Holiday_type::all();
        return view('admin.holidays.single',compact('holiday','types'));

    }
    public function search(Request $request){
        $start_date= $request->start_date;
        $end_date= $request->end_date;
        $holidays=Holiday::query();
        if ($start_date) {
            $holidays = $holidays->where('start', '>=', $start_date);
        }
        if ($end_date){
            $holidays=$holidays->where('start', '<=', $end_date);
        }
        $holidays=$holidays->with(['employee','hr','type'])->get();
        return json_encode($holidays);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
                'type' => 'required',
                'start' => 'required',
                'end' => 'required',

            ]
        );
        $holiday=Holiday::find($id);
        //delete old attendence
        $begin = new DateTime( $holiday->start );
        $end   = new DateTime( $holiday->end );
        for($i = $begin; $i <= $end; $i->modify('+1 day')){
            Attendence::where('employee_id',$holiday->employee->id)
                ->where('day',$i->format("Y-m-d"))->delete();

        }
        $holiday->update([
            'start'=>$request->start,
            'end'=>$request->end,
            'type_id'=>$request->type,

        ]);
        //delete absence if exist in this date
        $begin = new DateTime( $request->start );
        $end   = new DateTime( $request->end );
        for($i = $begin; $i <= $end; $i->modify('+1 day')){
            Attendence::where('employee_id',$holiday->employee->id)
                ->where('day',$i->format("Y-m-d"))->delete();
            // record status
            Attendence::create([
                'employee_id'=>$holiday->employee->id,
                'status'=>'أجازة',
                'day'=>$i->format("Y-m-d"),
                'hr_id'=>auth()->id()
            ]);
        }

        return redirect('/admin/holidays')->with('success', 'تم تعديل الأجازه بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $holiday=Holiday::find($id);
        //delete old attendence
        $begin = new DateTime( $holiday->start );
        $end   = new DateTime( $holiday->end );
        for($i = $begin; $i <= $end; $i->modify('+1 day')){
            Attendence::where('employee_id',$holiday->employee->id)
                ->where('day',$i->format("Y-m-d"))->delete();

        }
        $holiday->delete();
    }


}
