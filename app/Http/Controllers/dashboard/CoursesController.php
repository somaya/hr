<?php

namespace App\Http\Controllers\dashboard;

use App\Exports\CoursesExport;
use App\Http\Controllers\Controller;
use App\Models\Attendence;
use App\Models\Course;
use App\Models\Employee;
use DateTime;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class CoursesController extends Controller
{
    public function index()
    {
        $courses=Course::OrderBy('created_at','desc')->get();
        return view('admin.courses.index',compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $employee=Employee::find($id);

        return view('admin.courses.single',compact('employee'));
    }

    public function show()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
                'title' => 'required',
                'start' => 'required',
                'end' => 'required',
                'location' => 'required',
                'donor' => 'required',

            ]
        );
        Course::create([
            'employee_id'=>$request->employee,
            'start'=>$request->start,
            'title'=>$request->title,
            'end'=>$request->end,
            'location'=>$request->location,
            'donor'=>$request->donor,
            'hr_id'=>auth()->id(),
        ]);
        //delete absence if exist in this date
        $begin = new DateTime( $request->start );
        $end   = new DateTime( $request->end );
        for($i = $begin; $i <= $end; $i->modify('+1 day')){
            Attendence::where('employee_id',$request->employee)
                ->where('day',$i->format("Y-m-d"))->delete();
            // record status
            Attendence::create([
                'employee_id'=>$request->employee,
                'status'=>'دورة',
                'day'=>$i->format("Y-m-d"),
                'hr_id'=>auth()->id()
            ]);
        }
        return redirect('/admin/employees')->with('success', 'تم اضافة الدورة بنجاح');
    }
    public function export()
    {
        return Excel::download(new CoursesExport, 'courses.xlsx');
    }
    public function pdf(){
        $courses= Course::with(['employee','hr'])->get();


        $pdf = PDF::loadView('admin.courses.pdf', compact('courses'));
        return $pdf->stream('courses.pdf');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function edit($id)
    {
        $course=Course::find($id);
        return view('admin.courses.single',compact('course'));

    }
    public function search(Request $request){
        $start_date= $request->start_date;
        $end_date= $request->end_date;
        $courses=Course::query();
        if ($start_date) {
            $courses = $courses->where('start', '>=', $start_date);
        }
        if ($end_date){
            $courses=$courses->where('start', '<=', $end_date);
        }
        $courses=$courses->with(['employee','hr'])->get();
        return json_encode($courses);

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
                'title' => 'required',
                'start' => 'required',
                'end' => 'required',
                'location' => 'required',
                'donor' => 'required',

            ]
        );
        $course=Course::find($id);
        //delete old attendence
        $begin = new DateTime( $course->start );
        $end   = new DateTime( $course->end );
        for($i = $begin; $i <= $end; $i->modify('+1 day')){
            Attendence::where('employee_id',$course->employee->id)
                ->where('day',$i->format("Y-m-d"))->delete();

        }

        $course->update([
            'title'=>$request->title,
            'start'=>$request->start,
            'end'=>$request->end,
            'location'=>$request->location,
            'donor'=>$request->donor,

        ]);
        //delete absence if exist in this date
        $begin = new DateTime( $request->start );
        $end   = new DateTime( $request->end );
        for($i = $begin; $i <= $end; $i->modify('+1 day')){
            Attendence::where('employee_id',$course->employee->id)
                ->where('day',$i->format("Y-m-d"))->delete();
            // record status
            Attendence::create([
                'employee_id'=>$course->employee->id,
                'status'=>'دورة',
                'day'=>$i->format("Y-m-d"),
                'hr_id'=>auth()->id()
            ]);
        }

        return redirect('/admin/courses')->with('success', 'تم تعديل الدورة بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course=Course::find($id);
        //delete old attendence
        $begin = new DateTime( $course->start );
        $end   = new DateTime( $course->end );
        for($i = $begin; $i <= $end; $i->modify('+1 day')){
            Attendence::where('employee_id',$course->employee->id)
                ->where('day',$i->format("Y-m-d"))->delete();

        }
        $course->delete();
    }
}
