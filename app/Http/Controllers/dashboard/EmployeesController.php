<?php

namespace App\Http\Controllers\dashboard;

use App\Models\Attendence;
use App\Models\Employee;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees=Employee::OrderBy('created_at','desc')->get();
        return view('admin.employees.index',compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.employees.single');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
                'name' => 'required',
                'title' => 'required',

            ]
        );
        Employee::create([
            'name'=>$request->name,
            'title'=>$request->title,
            'hr_id'=>auth()->id(),
        ]);
        return redirect('/admin/employees')->with('success', 'تم اضافة الموظف بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function login($id)
    {
        date_default_timezone_set('Asia/Riyadh');
        $timeIn = Carbon::createFromFormat('H:i', '08:00'); //08:00 am
        $login = Carbon::createFromFormat('H:i', Carbon::now()->format('H:i'));

        $delay = $timeIn->diffInMinutes($login);
        Attendence::create([
            'day'=>Carbon::now()->format('Y-m-d'),
            'attend'=>1,
            'employee_id'=>$id,
            'hr_id'=>auth()->id(),
            'login'=>$login,
            'delay'=>$delay+30
        ]);
        return redirect('/admin/employees')->with('success', 'تم تسجيل حضور الموظف بنجاح');

    }
    public function logout($id)
    {
        date_default_timezone_set('Asia/Riyadh');
        $logout = Carbon::createFromFormat('H:i', Carbon::now()->format('H:i'));
        $attend=Attendence::where('day',Carbon::now()->format('Y-m-d'))
            ->where('employee_id',$id)->first();
        $attend->update(['logout'=>$logout]);


        return redirect('/admin/employees')->with('success', 'تم تسجيل انصراف الموظف بنجاح');

    }
    public function absent($id)
    {

        Attendence::create([
            'day'=>Carbon::now()->format('Y-m-d'),
            'attend'=>2,
            'employee_id'=>$id,
            'hr_id'=>auth()->id(),
        ]);
        return redirect('/admin/employees')->with('success', 'تم تسجيل غياب الموظف بنجاح');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee=Employee::find($id);
        return view('admin.employees.single',compact('employee'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
                'name' => 'required',
                'title' => 'required',
            ]
        );
        $employee=Employee::find($id);
        $employee->update([
            'name'=>$request->name,
            'title'=>$request->title,

        ]);

        return redirect('/admin/employees')->with('success', 'تم تعديل الموظف بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Employee::destroy($id);
    }
}
