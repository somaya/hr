<?php

namespace App\Http\Controllers\dashboard;

use App\Exports\TasksExport;
use App\Http\Controllers\Controller;
use App\Models\Attendence;
use App\Models\Employee;
use App\Models\Task;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
class TasksController extends Controller
{
    public function index()
    {
        $tasks=Task::OrderBy('created_at','desc')->get();
        return view('admin.tasks.index',compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $employee=Employee::find($id);

        return view('admin.tasks.single',compact('employee'));
    }
    public function show()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request, [
                'location' => 'required',
                'start' => 'required',
                'number_of_days' => 'required',

            ]
        );
        Task::create([
            'employee_id'=>$request->employee,
            'location'=>$request->location,
            'start'=>$request->start,
            'number_of_days'=>$request->number_of_days,
            'hr_id'=>auth()->id(),
        ]);
        //delete absence if exist in this date
        $begin = new DateTime( $request->start );
        $end   = Carbon::parse( $request->start)->addDays($request->number_of_days);
        for($i = $begin; $i < $end; $i->modify('+1 day')){
            Attendence::where('employee_id',$request->employee)
                ->where('day',$i->format("Y-m-d"))->delete();
            // record status
            Attendence::create([
                'employee_id'=>$request->employee,
                'status'=>'مهمة عمل',
                'day'=>$i->format("Y-m-d"),
                'hr_id'=>auth()->id()
            ]);
        }
        return redirect('/admin/employees')->with('success', 'تم اضافة مهمة العمل بنجاح');
    }
    public function export()
    {
        return Excel::download(new TasksExport, 'tasks.xlsx');
    }
    public function pdf(){
        $tasks= Task::with(['employee','hr'])->get();


        $pdf = PDF::loadView('admin.tasks.pdf', compact('tasks'));
        return $pdf->stream('tasks.pdf');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function edit($id)
    {
        $task=Task::find($id);
        return view('admin.tasks.single',compact('task'));

    }
    public function search(Request $request){
        $start_date= $request->start_date;
        $end_date= $request->end_date;
        $tasks=Task::query();
        if ($start_date) {
            $tasks = $tasks->where('start', '>=', $start_date);
        }
        if ($end_date){
            $tasks=$tasks->where('start', '<=', $end_date);
        }
        $tasks=$tasks->with(['employee','hr'])->get();
        return json_encode($tasks);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
                'location' => 'required',
                'start' => 'required',
                'number_of_days' => 'required',

            ]
        );
        $task=Task::find($id);
        //delete old attendence
        $begin = new DateTime( $task->start );
        $end   = Carbon::parse( $task->start)->addDays($request->number_of_days);
        for($i = $begin; $i < $end; $i->modify('+1 day')){
            Attendence::where('employee_id',$task->employee->id)
                ->where('day',$i->format("Y-m-d"))->delete();


        }
        $task->update([
            'start'=>$request->start,
            'number_of_days'=>$request->number_of_days,
            'location'=>$request->location,

        ]);
        //delete absence if exist in this date
        $begin = new DateTime( $request->start );
        $end   = Carbon::parse( $request->start)->addDays($request->number_of_days);
        for($i = $begin; $i < $end; $i->modify('+1 day')){
            Attendence::where('employee_id',$task->employee->id)
                ->where('day',$i->format("Y-m-d"))->delete();
            // record status
            Attendence::create([
                'employee_id'=>$task->employee->id,
                'status'=>'مهمة عمل',
                'day'=>$i->format("Y-m-d"),
                'hr_id'=>auth()->id()
            ]);

        }

        return redirect('/admin/tasks')->with('success', 'تم تعديل مهمه العمل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task=Task::find($id);
        //delete old attendence
        $begin = new DateTime( $task->start );
        $end   = Carbon::parse( $task->start)->addDays($request->number_of_days);
        for($i = $begin; $i < $end; $i->modify('+1 day')){
            Attendence::where('employee_id',$task->employee->id)
                ->where('day',$i->format("Y-m-d"))->delete();


        }
        $task->delete();
    }
}
