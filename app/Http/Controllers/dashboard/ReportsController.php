<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use App\Models\Attendence;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportsController extends Controller
{
    public function index()
    {
        $all_absents=Attendence::whereMonth('day', date('m'))->whereYear('day', date('Y'))->where('attend',2)->get();
//        $employee_absences=Attendence::whereMonth('day', date('m'))->whereYear('day', date('Y'))->where('attend',0)->get()->groupBy('employee_id');
        $employee_absences=DB::table('attendences')
            ->join('employees', 'employees.id', '=', 'attendences.employee_id')
                ->select('employee_id','employees.name', DB::raw('count(*) as total'))
                ->whereMonth('day', date('m'))->whereYear('day', date('Y'))->where('attend',2)
                ->groupBy('employee_id')
                ->get();
        $employee_delays=DB::table('attendences')
            ->join('employees', 'employees.id', '=', 'attendences.employee_id')
            ->select('employee_id','employees.name', DB::raw('sum(delay) as total'))
            ->whereMonth('day', date('m'))->whereYear('day', date('Y'))->where('attend',1)
            ->groupBy('employee_id')
            ->get();

        return view('admin.reports.index',compact('all_absents','employee_absences','employee_delays'));
    }
}
