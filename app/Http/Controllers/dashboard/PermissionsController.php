<?php

namespace App\Http\Controllers\dashboard;

use App\Exports\PermissionsExport;
use App\Http\Controllers\Controller;
use App\Models\Attendence;
use App\Models\Employee;
use App\Models\Permission;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class PermissionsController extends Controller
{
    public function index()
    {
        $permissions=Permission::OrderBy('created_at','desc')->get();
        return view('admin.permissions.index',compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $employee=Employee::find($id);

        return view('admin.permissions.single',compact('employee'));
    }

    public function show()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
                'type' => 'required',
                'date' => 'required',

            ]
        );
        Permission::create([
            'employee_id'=>$request->employee,
            'type'=>$request->type,
            'date'=>$request->date,
            'hr_id'=>auth()->id(),
        ]);
        //delete absence if exist in this date
        Attendence::where('employee_id',$request->employee)
            ->where('day',$request->date)->delete();
        // record status
        Attendence::create([
            'employee_id'=>$request->employee,
            'status'=>'استئذان',
            'day'=>$request->date,
            'hr_id'=>auth()->id()
        ]);

        return redirect('/admin/employees')->with('success', 'تم اضافة الاستئذان بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function edit($id)
    {
        $permission=Permission::find($id);
        return view('admin.permissions.single',compact('permission'));

    }
    public function search(Request $request){
    $start_date= $request->start_date;
    $end_date= $request->end_date;
    $permissions=Permission::query();
    if ($start_date) {
        $permissions = $permissions->where('date', '>=', $start_date);
    }
    if ($end_date){
        $permissions=$permissions->where('date', '<=', $end_date);
    }
    $permissions=$permissions->with(['employee','hr'])->get();
    return json_encode($permissions);

    }
    public function export()
    {
        return Excel::download(new PermissionsExport, 'permissions.xlsx');
    }
    public function pdf(){
       $permissions= Permission::with(['employee','hr'])->get();


        $pdf = PDF::loadView('admin.permissions.pdf', compact('permissions'));
        return $pdf->stream('permissions.pdf');


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
                'type' => 'required',
                'date' => 'required',

            ]
        );
        $permission=Permission::find($id);
        //delete old attendence
        Attendence::where('employee_id',$permission->employee->id)
            ->where('day',$permission->date)->delete();
        $permission->update([
            'date'=>$request->date,
            'type'=>$request->type,

        ]);
        //delete absence if exist in this date
        Attendence::where('employee_id',$permission->employee->id)
            ->where('day',$request->date)->delete();
        // record status
        Attendence::create([
            'employee_id'=>$permission->employee->id,
            'status'=>'استئذان',
            'day'=>$request->date,
            'hr_id'=>auth()->id()
        ]);

        return redirect('/admin/permissions')->with('success', 'تم تعديل الاستئذان بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission=Permission::find($id);
        //delete old attendence
        Attendence::where('employee_id',$permission->employee->id)
            ->where('day',$permission->date)->delete();
        $permission->delete();
    }
}
