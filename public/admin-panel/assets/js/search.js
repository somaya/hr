
$(document).ready(function () {
    getPermissions();
    getHolidays();
    getCourses();
    getTasks();


    function getPermissions() {
        $("#permission_search_button").on('click',function (e) {
            var start_date = $("#start_date").val();
            var end_date = $("#end_date").val();

            console.log( start_date,end_date);


            $.ajax({
                url: "/getPermissionResult?start_date=" + start_date+"&end_date="+end_date,
                type: "get",
                dataType: "json",
//
                success: function (data)
                {
                    console.log('data',data);
                    document.getElementById('permission_tbl').innerHTML='';
                    var str='';
                    if (jQuery.isEmptyObject(data)){
                        str+='<span class="text-center" style="color:red;text-align:center">لا توجد نتائج</span>';

                    }else {
                        str +='<thead>'+
                        '<tr>'+
                        // '<th class="text-left">#</th>'+
                        '<th>إسم الموظف</th>'+
                        '<th>التاريخ</th>'+
                        '<th>مضاف بواسطة</th>'+
                        '<th>نوع الاستئذان</th>'+
                        '<th class="text-left">الاجراء المتخذ</th>'+
                        '</tr>'+
                        '</thead>'+
                        '<tbody>';
                        $.each(data, function (i, permission) {
                            str+='<tr>'+
                                // '<td>'+i+'</td>'+
                            '<td>'+permission.employee.name+' </td>'+
                            '<td>'+permission.date+'</td>'+
                            '<td>'+permission.hr.name+'</td>'+
                            '<td>'+permission.type+'</td>'+


                            '<td class="text-center">'+
                                '<ul class="icons-list">'+
                                '<li class="text-primary-600">'+
                                '<a title="تعديل" href="/admin/permissions/'+permission.id+'/edit"><i class="icon-pencil7"></i></a>'+
                                '</li>'+


                                '<li class="text-danger-600">'+
                                '<a title="حذف الاستئذان" onclick="return false;" object_id="'+permission.id+'" delete_url="/admin/permissions/'+permission.id+'"class="sweet_warning delete_user" href="#">'+
                                '<i class="icon-trash"></i></a>'+
                                '</li>'+

                                '</ul>'+
                                '</td>'+

                                '</tr>';

                        });
                        str+='</tbody> </table>';
                    }
                    $('#permission_tbl').append(str);

                }
            });
        });
    }
    function getHolidays() {
        $("#holiday_search_button").on('click',function (e) {
            var start_date = $("#start_date").val();
            var end_date = $("#end_date").val();

            console.log( start_date,end_date);


            $.ajax({
                url: "/getHolidayResult?start_date=" + start_date+"&end_date="+end_date,
                type: "get",
                dataType: "json",
//
                success: function (data)
                {
                    console.log('data',data);
                    document.getElementById('holiday_tbl').innerHTML='';
                    var str='';
                    if (jQuery.isEmptyObject(data)){
                        str+='<span class="text-center" style="color:red;text-align:center">لا توجد نتائج</span>';

                    }else {
                        str +='<thead>'+
                        '<tr>'+
                        // '<th class="text-left">#</th>'+
                        '<th>إسم الموظف</th>'+
                        '<th>تاريخ البدء</th>'+
                        '<th>تاريخ النهايه</th>'+
                        '<th>مضاف بواسطة</th>'+
                        '<th>نوع الأجازه</th>'+
                        '<th class="text-left">الاجراء المتخذ</th>'+
                        '</tr>'+
                        '</thead>'+
                        '<tbody>';
                        $.each(data, function (i, holiday) {
                            str+='<tr>'+
                                // '<td>'+i+'</td>'+
                            '<td>'+holiday.employee.name+' </td>'+
                            '<td>'+holiday.start+'</td>'+
                            '<td>'+holiday.end+'</td>'+
                            '<td>'+holiday.hr.name+'</td>'+
                            '<td>'+holiday.type.name+'</td>'+


                            '<td class="text-center">'+
                                '<ul class="icons-list">'+
                                '<li class="text-primary-600">'+
                                '<a title="تعديل" href="/admin/holidays/'+holiday.id+'/edit"><i class="icon-pencil7"></i></a>'+
                                '</li>'+


                                '<li class="text-danger-600">'+
                                '<a title="حذف الاجازة" onclick="return false;" object_id="'+holiday.id+'" delete_url="/admin/holidays/'+holiday.id+'"class="sweet_warning delete_user" href="#">'+
                                '<i class="icon-trash"></i></a>'+
                                '</li>'+

                                '</ul>'+
                                '</td>'+

                                '</tr>';

                        });
                        str+='</tbody> </table>';
                    }
                    $('#holiday_tbl').append(str);

                }
            });
        });
    }
    function getCourses() {
        $("#course_search_button").on('click',function (e) {
            var start_date = $("#start_date").val();
            var end_date = $("#end_date").val();

            console.log( start_date,end_date);


            $.ajax({
                url: "/getCourseResult?start_date=" + start_date+"&end_date="+end_date,
                type: "get",
                dataType: "json",
//
                success: function (data)
                {
                    console.log('data',data);
                    document.getElementById('course_tbl').innerHTML='';
                    var str='';
                    if (jQuery.isEmptyObject(data)){
                        str+='<span class="text-center" style="color:red;text-align:center">لا توجد نتائج</span>';

                    }else {
                        str +='<thead>'+
                        '<tr>'+
                        // '<th class="text-left">#</th>'+
                        '<th>إسم الموظف</th>'+
                        '<th>الدوره</th>'+
                        '<th>تاريخ البدء</th>'+
                        '<th>تاريخ النهايه</th>'+
                        '<th>مكان الدوره</th>'+
                        '<th>الجهه المانحه</th>'+
                        '<th>مضاف بواسطة</th>'+
                        '<th class="text-left">الاجراء المتخذ</th>'+
                        '</tr>'+
                        '</thead>'+
                        '<tbody>';
                        $.each(data, function (i, course) {
                            str+='<tr>'+
                                // '<td>'+i+'</td>'+
                            '<td>'+course.employee.name+' </td>'+
                            '<td>'+course.title+' </td>'+
                            '<td>'+course.start+'</td>'+
                            '<td>'+course.end+'</td>'+
                            '<td>'+course.location+'</td>'+
                            '<td>'+course.donor+'</td>'+
                            '<td>'+course.hr.name+'</td>'+


                            '<td class="text-center">'+
                                '<ul class="icons-list">'+
                                '<li class="text-primary-600">'+
                                '<a title="تعديل" href="/admin/courses/'+course.id+'/edit"><i class="icon-pencil7"></i></a>'+
                                '</li>'+


                                '<li class="text-danger-600">'+
                                '<a title="حذف الدوره" onclick="return false;" object_id="'+course.id+'" delete_url="/admin/courses/'+course.id+'"class="sweet_warning delete_user" href="#">'+
                                '<i class="icon-trash"></i></a>'+
                                '</li>'+

                                '</ul>'+
                                '</td>'+

                                '</tr>';

                        });
                        str+='</tbody> </table>';
                    }
                    $('#course_tbl').append(str);

                }
            });
        });
    }
    function getTasks() {
        $("#task_search_button").on('click',function (e) {
            var start_date = $("#start_date").val();
            var end_date = $("#end_date").val();

            console.log( start_date,end_date);


            $.ajax({
                url: "/getTaskResult?start_date=" + start_date+"&end_date="+end_date,
                type: "get",
                dataType: "json",
//
                success: function (data)
                {
                    console.log('data',data);
                    document.getElementById('task_tbl').innerHTML='';
                    var str='';
                    if (jQuery.isEmptyObject(data)){
                        str+='<span class="text-center" style="color:red;text-align:center">لا توجد نتائج</span>';

                    }else {
                        str +='<thead>'+
                        '<tr>'+
                        // '<th class="text-left">#</th>'+
                        '<th>إسم الموظف</th>'+
                        '<th>تاريخ المهمه</th>'+
                        '<th>عدد الأيام</th>'+
                        '<th>مكان المهمه</th>'+
                        '<th>مضاف بواسطة</th>'+
                        '<th class="text-left">الاجراء المتخذ</th>'+
                        '</tr>'+
                        '</thead>'+
                        '<tbody>';
                        $.each(data, function (i, task) {
                            str+='<tr>'+
                                // '<td>'+i+'</td>'+
                            '<td>'+task.employee.name+' </td>'+
                            '<td>'+task.start+'</td>'+
                            '<td>'+task.number_of_days+'</td>'+
                            '<td>'+task.location+'</td>'+
                            '<td>'+task.hr.name+'</td>'+


                            '<td class="text-center">'+
                                '<ul class="icons-list">'+
                                '<li class="text-primary-600">'+
                                '<a title="تعديل" href="/admin/tasks/'+task.id+'/edit"><i class="icon-pencil7"></i></a>'+
                                '</li>'+


                                '<li class="text-danger-600">'+
                                '<a title="حذف مهمة العمل" onclick="return false;" object_id="'+task.id+'" delete_url="/admin/tasks/'+task.id+'"class="sweet_warning delete_user" href="#">'+
                                '<i class="icon-trash"></i></a>'+
                                '</li>'+

                                '</ul>'+
                                '</td>'+

                                '</tr>';

                        });
                        str+='</tbody> </table>';
                    }
                    $('#task_tbl').append(str);

                }
            });
        });
    }

});