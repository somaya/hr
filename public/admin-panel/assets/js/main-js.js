$(document).ready(function(){
    // Search
    $('.search .search-button').click(function(){
        $(this).parent().toggleClass('open');
        $('.open input').focus();
    });

});

// delete single item from group
$(document).on( "click", ".sweet_warning", function() {
    console.log('enter');
    // $('form').on('click', '.sweet_warning', function() {
//
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })

    var id = $(this).attr('object_id');
    console.log(id);
    var d_url = $(this).attr('delete_url');
    var elem = $(this).parent().parent().parent().parent();

    var token = $('meta[name="_token"]').attr('content');
    swal({
            title: "هل انت متأكد ؟",
            text: "سيتم الحذف بشكل نهائي ولن تكون قادرا على استعادة المعلومات",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "نعم ، قم بالحذف !",
            cancelButtonText: "الغاء الحذف",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm) {

                $.ajax({
                    url : d_url,
                    type : "DELETE",
                    data : "",
                    success : function(result) {

                    }
                });
                elem.hide(1000);
                swal({
                    title: "تم الحذف ",
                    text: "نعم !",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
                });
            }
            else {
                swal({
                    title: "تم الغاء الطلب",
                    text: "تم الغاء طلب الحذف بنجاح !",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
                });
            }
        });
});