<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {
    Route::resource('/types', 'dashboard\TypesController');
    Route::resource('/employees', 'dashboard\EmployeesController');
    Route::get('/employees/{id}/login', 'dashboard\EmployeesController@login');
    Route::get('/employees/{id}/logout', 'dashboard\EmployeesController@logout');
    Route::get('/employees/{id}/absent', 'dashboard\EmployeesController@absent');
    Route::resource('/admins', 'dashboard\AdminsController');
    Route::resource('/holidays', 'dashboard\HolidaysController', ['except' => [ 'create' ]]);
    Route::get('/holidays/{id}/create', 'dashboard\HolidaysController@create');
    Route::post('/holidays/store', 'dashboard\HolidaysController@store');
    Route::resource('/permissions', 'dashboard\PermissionsController', ['except' => [ 'create' ]]);
    Route::get('/permissions/{id}/create', 'dashboard\PermissionsController@create');
    Route::post('/permissions/store', 'dashboard\PermissionsController@store');
    Route::resource('/courses', 'dashboard\CoursesController', ['except' => [ 'create' ]]);
    Route::get('/courses/{id}/create', 'dashboard\CoursesController@create');
    Route::post('/courses/store', 'dashboard\CoursesController@store');
    Route::resource('/tasks', 'dashboard\TasksController', ['except' => [ 'create' ]]);
    Route::get('/tasks/{id}/create', 'dashboard\TasksController@create');
    Route::post('/tasks/store', 'dashboard\TasksController@store');
    Route::get('/dashboard', 'DashboardsController@index');
    Route::get('/reports', 'dashboard\ReportsController@index');
    Route::get('/permission/export', 'dashboard\PermissionsController@export');
    Route::get('/course/export', 'dashboard\CoursesController@export');
    Route::get('/task/export', 'dashboard\TasksController@export');
    Route::get('/holiday/export', 'dashboard\HolidaysController@export');
    Route::get('/permission/pdf', 'dashboard\PermissionsController@pdf');
    Route::get('/course/pdf', 'dashboard\CoursesController@pdf');
    Route::get('/task/pdf', 'dashboard\TasksController@pdf');
    Route::get('/holiday/pdf', 'dashboard\HolidaysController@pdf');

});

Route::get('/getPermissionResult', 'dashboard\PermissionsController@search');
Route::get('/getHolidayResult', 'dashboard\HolidaysController@search');
Route::get('/getCourseResult', 'dashboard\CoursesController@search');
Route::get('/getTaskResult', 'dashboard\TasksController@search');

